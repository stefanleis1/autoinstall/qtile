#!/usr/bin/env bash

echo "This is the configuration script for Arch on a NVME-SSD"
echo "-------------------------------------------------"
echo "Setting up mirrors for optimal download - DE Only"
echo "-------------------------------------------------"
timedatectl set-ntp true
pacman -S --noconfirm pacman-contrib
echo -e "\nInstalling prereqs...\n$HR"
pacman -S --noconfirm gptfdisk btrfs-progs
chmod a+x qtile/btrfs/btrfs_baseinstall.sh

echo "-------------------------------------------------"
echo "-------select your disk to format----------------"
echo "-------------------------------------------------"
lsblk -f
read -p "Please enter disk [/dev/sda]:" DISK
DISK=${DISK:-/dev/sda}
echo "--------------------------------------"
echo -e "\nFormatting disk...\n$HR"
echo "--------------------------------------"
#################### Disk Prep ###########################################################
sgdisk -Z ${DISK} # zap all on disk
sgdisk -a 2048 -o ${DISK} # new gpt disk 2048 alignment
#################### Create Partitions ###################################################
sgdisk -n 1:0:+500M ${DISK} # partition 1 (UEFI SYS), default start block, 512MB
sgdisk -n 2:0:+500M ${DISK} # partition 2 (Boot),
sgdisk -n 3:0:0     ${DISK} # partition 3 (LVM), with root and home partition later on
#################### Set Partition Types #################################################
sgdisk -t 1:ef00 ${DISK}
sgdisk -t 2:8300 ${DISK}
sgdisk -t 3:8300 ${DISK}
#################### Label Partitions ####################################################
sgdisk -c 1:"UEFISYS" ${DISK}
sgdisk -c 2:"BOOT"    ${DISK}
sgdisk -c 3:"BTRFS"   ${DISK}
#################### LUKS encryption ###################################################### 
cryptsetup luksFormat --type luks1 -v --key-size 512 --hash sha256 --iter-time 500 --use-random ${DISK}2
cryptsetup open ${DISK}2 luks-btrfs-boot 
cryptsetup luksFormat -v --key-size 512 --hash sha256 --iter-time 200 --use-random ${DISK}3 
cryptsetup open ${DISK}3 luks-btrfs-root

mkfs.vfat -F32 -n "UEFISYS" ${DISK}1
mkfs.btrfs /dev/mapper/luks-btrfs-boot 
mkfs.btrfs /dev/mapper/luks-btrfs-root
################### Mount Target && BTRFS Setup  ############################################################
mount /dev/mapper/luks-btrfs-root /mnt
cd /mnt
btrfs subvolume create @
btrfs subvolume create @home
btrfs subvolume create @snapshots
btrfs subvolume create @var
cd
umount /mnt
mount -o noatime,compress=zstd,space_cache=v2,discard=async,subvol=@ /dev/mapper/luks-btrfs-root /mnt
mkdir /mnt/home
mount -o noatime,compress=zstd,space_cache=v2,discard=async,subvol=@home /dev/mapper/luks-btrfs-root /mnt/home
mkdir /mnt/.snapshots
mount -o noatime,compress=zstd,space_cache=v2,discard=async,subvol=@snapshots /dev/mapper/luks-btrfs-root /mnt/.snapshots
mkdir /mnt/var
mount -o noatime,compress=zstd,space_cache=v2,discard=async,subvol=@var /dev/mapper/luks-btrfs-root /mnt/var
mkdir /mnt/boot
mount /dev/mapper/luks-btrfs-boot /mnt/boot
mkdir /mnt/boot/EFI
mount ${DISK}1 /mnt/boot/EFI
mkdir /mnt/etc
# ################### Generating Fstab-mount Directory #########################################
genfstab -U /mnt >> /mnt/etc/fstab
# ################### Installing Some Needed Basic Packages For Further Installation ###########
pacstrap /mnt base base-devel linux-lts linux-lts-headers linux-lts-docs linux linux-headers linux-docs linux-firmware vim nano sudo git btrfs-progs --noconfirm --needed 
cp -r qtile /mnt
arch-chroot /mnt ./qtile/btrfs/btrfs_baseinstall.sh
 
