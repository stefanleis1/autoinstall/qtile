
#!/usr/bin/env bash

echo -e "\nInstalling Base System\n"

PKGS=(

    # --- XORG Display Rendering
        'xorg'                  # Base Package
        'xorg-drivers'          # Display Drivers 
        'xterm'                 # Terminal for TTY
        'xorg-server'           # Xorg server
        'xorg-apps'             # Xorg apps group
        'xorg-xinit'            # Xorg init
        'xorg-xinput'           # Xorg xinput
        'xorg-server-xephyr'
        'mesa'                  # Open source version of OpenGL

    # --- Setup Desktop
        'sddm'
        'cinnamon'              # Awesome Desktop
        'qtile'                 # Qtile Window Manager
        'inter-font'
        'xfce4-power-manager'   # Power Manager 
        'dmenu'                  # Menu System
        'picom'                 # Translucent Windows
        'xclip'                 # System Clipboard
        'lxappearance'          # Set System Themes
        'flatpak'               # Additional package source
        'firefox'               # Webbrowser
        'keepassxc'             # Password Safe
        'signal-desktop'        # WhatsApp Alternative
        'gnome-calculator'
        'dmenu'     

    # --- Filemanager
         'pcmanfm'
         'cinnamon-translations'
         'evince'                   # PDF viewer

    # --- Login Display Manager
        'sddm'
        
    # --- Networking Setup
        'network-manager-applet'    # System tray icon/utility for network connectivity
        'libsecret'                 # Library for storing passwords
        'iwd'                       # WLAN tool
        'git'
            
    # --- Audio
        'alsa-utils'        # Advanced Linux Sound Architecture (ALSA) Components https://alsa.opensrc.org/
        'alsa-plugins'      # ALSA plugins
        'pulseaudio'        # Pulse Audio sound components
        'pulseaudio-alsa'   # ALSA configuration for pulse audio
        'pavucontrol'       # Pulse Audio volume control
        'pnmixer'           # System tray volume control
        'clementine'

    # --- Bluetooth
        'bluez'                 # Daemons for the bluetooth protocol stack
        'bluez-utils'           # Bluetooth development and debugging utilities
        'bluez-firmware'        # Firmwares for Broadcom BCM203x and STLC2300 Bluetooth chips
        'blueberry'             # Bluetooth configuration tool
        'pulseaudio-bluetooth'  # Bluetooth support for PulseAudio
    
    # --- Printers
        'cups'                  # Open source printer drivers
        'cups-pdf'              # PDF support for cups
        'ghostscript'           # PostScript interpreter
        'gsfonts'               # Adobe Postscript replacement fonts
        'hplip'                 # HP Drivers
        'system-config-printer' # Printer setup  utility

    # SYSTEM --------------------------------------------------------------

    'thunderbird'           # Mailclient
    'grub-customizer'       # Grub Customizing Tool
    'fail2ban'              #
    'lutris'                # Gaming Utility
    'redshift'              # Utility For Nightime Lighting
    'notepadqq'
    'pacman-contrib'
    # TERMINAL UTILITIES --------------------------------------------------
    'bpytop'                # All-in-one CLI System Stats Tool
    'bashtop'
    'bash-completion'       # Tab Completion For Bash
    'betterlockscreen'
    'bleachbit'             # File Deletion Utility
    'brightnessctl'
    'cronie'                # Cron Jobs
    'curl'                  # Remote content retrieval
    'dmidecode'             #
    'file-roller'           # Archive Utility
    'gtop'                  # System Monitoring Via Terminal
    'gufw'                  # Firewall Manager GUI
    'ufw'                   # Firewall
    'hardinfo'              # Hardware Info App
    'htop'                  # Process Viewer
    'hddtemp'               # Disk Temp CLI Tool
    'lm_sensors'            # CLI Tool For Reading Temperature Sensors
    'net-tools'             # Network CLI Tool
    'neofetch'              # Shows system info when you launch terminal
    'ntp'                   # Network Time Protocol to set time via network.
    'numlockx'              # Turns on numlock in X11
    'openssh'               # SSH connectivity tools
    'p7zip'                 # 7z compression program
    'rsync'                 # Remote file sync utility
    'speedtest-cli'         # Internet speed via terminal
    'terminus-font'         # Font package with some bigger fonts for login terminal
    'tlp'                   # Advanced laptop power management
    'unrar'                 # RAR compression program
    'unzip'                 # Zip compression program
    'wget'                  # Remote content retrieval
    'terminator'            # Terminal emulator
    'vim'                   # Terminal Editor
    'zenity'                # Display graphical dialog boxes via shell scripts
    'zip'                   # Zip compression program
    'zsh'                   # ZSH shell
    'zsh-completions'       # Tab completion for ZSH
    'zsh-autosuggestions'
    'zsh-syntax-highlighting'
    'arch-wiki-lite'        # Arch Wiki In CLI
    'fd'                    # find command replacement
    'bat'                   # cat command replacement
    'ripgrep'               # grep command replacement
    'pywal'                 # custom Terminal color changer 

    # DISK UTILITIES ------------------------------------------------------
    
    'autofs'                # Auto-mounter
    'baobab'
    'btrfs-progs'           # BTRFS Support
    'dosfstools'            # DOS Support
    'exfat-utils'           # Mount exFat drives
    'f2fs-tools'            # Program for SSD-Diskfiles
    'gparted'               # Disk utility
    'gvfs-mtp'              # Read MTP Connected Systems
    'gvfs-smb'              # More File System Stuff
    'ntfs-3g'               # Open source implementation of NTFS file system
    'parted'                # Disk utility
    'samba'                 # Samba File Sharing
    'smartmontools'         # Disk Monitoring
    'smbclient'             # SMB Connection 
    'xfsprogs'              # XFS Support
    'exa'
    # GENERAL UTILITIES ---------------------------------------------------

    'flameshot'             # Screenshots
    'variety'               # Wallpaper changer
    'solaar'                # Logitech Unifying Connection tool for Linux
    'pamac'                 # Arch Software GUI
    'youtube-dl'
    
    # DEVELOPMENT ---------------------------------------------------------

    'geany'                 # Text editor
    'geany-plugins'         # PLugins for Geany
    'clang'                 # C Lang compiler
    'cmake'                 # Cross-platform open-source make system
    'electron'              # Cross-platform development using Javascript
    'git'                   # Version control system
    'gcc'                   # C/C++ compiler
    'glibc'                 # C libraries
    'meld'                  # File/directory comparison
    'nodejs'                # Javascript runtime environment
    'npm'                   # Node package manager
    'python'                # Scripting language
    'yarn'                  # Dependency management (Hyper needs this)
    'python-pip'
    'python3'
    

    # MEDIA ---------------------------------------------------------------
    'celluloid'             # Video player
    
    # GRAPHICS AND DESIGN -------------------------------------------------

    'gcolor2'               # Colorpicker
    'ristretto'             # Multi image viewer

    # PRODUCTIVITY --------------------------------------------------------

    'hunspell'              # Spellcheck libraries
    'hunspell-en'           # English spellcheck library
    'xpdf'                  # PDF viewer
)

for PKG in "${PKGS[@]}"; do
    echo "INSTALLING: ${PKG}"
    sudo pacman -S "$PKG" --noconfirm --needed
done

bash /qtile/btrfs/postinstall.sh
