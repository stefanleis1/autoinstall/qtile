#!/usr/bin/env bash

function install_program {
		echo "--- Installing progam: $1"
		sudo pacman -S $1 --noconfirm --needed
	}

#List of Applications:

# --- XORG Display Rendering
install_program 'xorg'                  # Base Package
install_program 'xorg-drivers'          # Display Drivers 
install_program 'xterm'                 # Terminal for TTY
install_program 'xorg-server'           # Xorg server
install_program 'xorg-apps'             # Xorg apps group
install_program 'xorg-xinit'            # Xorg init
install_program 'xorg-xinput'           # Xorg xinput
install_program 'xorg-server-xephyr'
install_program 'mesa'                  # Open source version of OpenGL
install_program 'sddm'
install_program 'cinnamon'              # Awesome Desktop
install_program 'qtile'                 # Qtile Window Manager
install_program 'inter-font'
install_program 'xfce4-power-manager'   # Power Manager 
install_program 'dmenu'                  # Menu System
install_program 'picom'                 # Translucent Windows
install_program 'xclip'                 # System Clipboard
install_program 'lxappearance'          # Set System Themes
install_program 'flatpak'               # Additional package source
install_program 'firefox'               # Webbrowser
install_program 'keepassxc'             # Password Safe
install_program 'signal-desktop'        # WhatsApp Alternative
install_program 'gnome-calculator'
install_program 'dmenu'     

    # --- Filemanager
install_program 'pcmanfm'
install_program 'cinnamon-translations'
install_program 'evince'                   # PDF viewer

    # --- Login Display Manager
install_program 'sddm'
install_program        
    # --- Networking Setup
install_program 'network-manager-applet'    # System tray icon/utility for network connectivity
install_program 'libsecret'                 # Library for storing passwords
install_program 'iwd'                       # WLAN tool
install_program 'git'
            
    # --- Audio
install_program 'alsa-utils'        
install_program 'alsa-plugins'      # ALSA plugins
install_program 'pulseaudio'        # Pulse Audio sound components
install_program 'pulseaudio-alsa'   # ALSA configuration for pulse audio
install_program 'pavucontrol'       # Pulse Audio volume control
install_program 'pnmixer'           # System tray volume control
install_program 'clementine'

    # --- Bluetooth
install_program 'bluez'                 # Daemons for the bluetooth protocol stack
install_program 'bluez-utils'           # Bluetooth development and debugging utilities
install_program 'bluez-firmware'        # Firmwares for Broadcom BCM203x and STLC2300 Bluetooth chips
install_program 'blueberry'             # Bluetooth configuration tool
install_program 'pulseaudio-bluetooth'  # Bluetooth support for PulseAudio
    
    # --- Printers
install_program 'cups'                  # Open source printer drivers
install_program 'cups-pdf'              # PDF support for cups
install_program 'ghostscript'           # PostScript interpreter
install_program 'gsfonts'               # Adobe Postscript replacement fonts
install_program 'hplip'                 # HP Drivers
install_program 'system-config-printer' # Printer setup  utility

    # SYSTEM --------------------------------------------------------------

install_program 'thunderbird'           # Mailclient
install_program 'grub-customizer'       # Grub Customizing Tool
install_program 'fail2ban'              #
install_program   'lutris'                # Gaming Utility
install_program 'redshift'              # Utility For Nightime Lighting
install_program 'notepadqq'
install_program 'pacman-contrib'
    # TERMINAL UTILITIES --------------------------------------------------
install_program 'bpytop'                # All-in-one CLI System Stats Tool
install_program 'bashtop'
install_program 'bash-completion'       # Tab Completion For Bash
install_program 'betterlockscreen'
install_program 'bleachbit'             # File Deletion Utility
install_program 'brightnessctl'
install_program 'cronie'                # Cron Jobs
install_program 'curl'                  # Remote content retrieval
install_program 'dmidecode'             #
install_program 'file-roller'           # Archive Utility
install_program 'gtop'                  # System Monitoring Via Terminal
install_program 'gufw'                  # Firewall Manager GUI
install_program 'ufw'                   # Firewall
install_program 'hardinfo'              # Hardware Info App
install_program 'htop'                  # Process Viewer
install_program 'hddtemp'               # Disk Temp CLI Tool
install_program 'lm_sensors'            # CLI Tool For Reading Temperature Sensors
install_program 'net-tools'             # Network CLI Tool
install_program 'neofetch'              # Shows system info when you launch terminal
install_program 'ntp'                   # Network Time Protocol to set time via network.
install_program 'numlockx'              # Turns on numlock in X11
install_program 'openssh'               # SSH connectivity tools
install_program 'p7zip'                 # 7z compression program
install_program 'rsync'                 # Remote file sync utility
install_program 'speedtest-cli'         # Internet speed via terminal
install_program 'terminus-font'         # Font package with some bigger fonts for login terminal
install_program 'tlp'                   # Advanced laptop power management
install_program 'unrar'                 # RAR compression program
install_program 'unzip'                 # Zip compression program
install_program 'wget'                  # Remote content retrieval
install_program 'terminator'            # Terminal emulator
install_program 'vim'                   # Terminal Editor
install_program 'zenity'                # Display graphical dialog boxes via shell scripts
install_program 'zip'                   # Zip compression program
install_program 'zsh'                   # ZSH shell
install_program 'zsh-completions'       # Tab completion for ZSH
install_program 'zsh-autosuggestions'
install_program 'zsh-syntax-highlighting'
install_program 'arch-wiki-lite'        # Arch Wiki In CLI
install_program 'fd'                    # find command replacement
install_program 'bat'                   # cat command replacement
install_program 'ripgrep'               # grep command replacement
install_program 'pywal'                 # custom Terminal color changer 

    # DISK UTILITIES ------------------------------------------------------
    
install_program 'autofs'                # Auto-mounter
install_program 'baobab'
install_program 'btrfs-progs'           # BTRFS Support
install_program 'dosfstools'            # DOS Support
install_program 'exfat-utils'           # Mount exFat drives
install_program 'f2fs-tools'            # Program for SSD-Diskfiles
install_program 'gparted'               # Disk utility
install_program 'gvfs-mtp'              # Read MTP Connected Systems
install_program   'ntfs-3g'               # Open source implementation of NTFS file system
install_program 'parted'                # Disk utility
install_program 'samba'                 # Samba File Sharing
install_program 'smartmontools'         # Disk Monitoring
install_program 'smbclient'             # SMB Connection 
install_program 'xfsprogs'              # XFS Support
install_program 'exa'
    # GENERAL UTILITIES ---------------------------------------------------

install_program 'flameshot'             # Screenshots
install_program 'variety'               # Wallpaper changer
install_program 'solaar'                # Logitech Unifying Connection tool for Linux
install_program 'pamac'                 # Arch Software GUI
install_program 'youtube-dl'
    
    # DEVELOPMENT ---------------------------------------------------------

install_program 'geany'                 # Text editor
install_program 'geany-plugins'         # PLugins for Geany
install_program 'clang'                 # C Lang compiler
install_program 'cmake'                 # Cross-platform open-source make system
install_program 'electron'              # Cross-platform development using Javascript
install_program 'git'                   # Version control system
install_program 'gcc'                   # C/C++ compiler
install_program 'glibc'                 # C libraries
install_program 'meld'                  # File/directory comparison
install_program 'nodejs'                # Javascript runtime environment
install_program 'npm'                   # Node package manager
install_program 'python'                # Scripting language
install_program 'yarn'                  # Dependency management (Hyper needs this)
install_program 'python-pip'
install_program'python3'
    

    # MEDIA ---------------------------------------------------------------
install_program 'celluloid'             # Video player
    
    # GRAPHICS AND DESIGN -------------------------------------------------

install_program 'gcolor2'               # Colorpicker
install_program 'ristretto'             # Multi image viewer

    # PRODUCTIVITY --------------------------------------------------------

install_program 'hunspell'              # Spellcheck libraries
install_program 'hunspell-en'           # English spellcheck library
install_program 'xpdf'                  # PDF viewer
install_program 'brave-bin'
install_program 'libadwaita'
install_program 'onlyoffice-bin'
install_program 'materia-gtk-theme'
install_program 'nodejs-nativefier'
install_program 'paru'
install_program 'papirus-icon-theme'
install_program 'plex-media-player'
install_program 'timeshift'
install_program 'timeshift-autosnap'
install_program 'visual-studio-code-bin'
install_program 'vmware-keymaps'
install_program 'whatsapp-nativefier'

