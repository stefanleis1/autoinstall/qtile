#!/usr/bin/env bash

echo -e "\nINSTALLING AUR SOFTWARE\n"

cd "${HOME}"

echo "CLOING: PIKAUR "
git clone "https://aur.archlinux.org/yay-bin.git"

PKGS=(
    'alacritty-colorscheme'
    'auto-cpufreq-git'
    'autojump'
    'autofs'
    'balena-etcher'
    'blight'
    'displayink'
    'evdi-git'
    'fslint'
    'gcalendar'
    'google-calendar-nativefier-dark'
    'i3lock-fancy'
    'kite'
    'light-git'
# Desktop Theme
    'meteo-gtk'
    'nerd-fonts-mononoki'
    'nerd-fonts-inconsolata'
    'nerd-fonts-cascadia-code'
# Desktop Icons
    'picom-jonaburg-git'
    'python-wall-steam-git'
    'shortwave'
    'vmware-workstation'
    'webapp-manager-git'
)

cd ${HOME}/yay-bin
makepkg -si --noconfirm

for PKG in "${PKGS[@]}"; do
    yay -S --noconfirm $PKG
done

echo -e "\nDone!\n"

#-------------------Customizations

# installing displayink

yay displaylink --noconfirm --needed

for pkg in $(find /var/cache/pacman/pkg/ -type f -iname "xorg*1.20.8-3*"); do sudo pacman -U $pkg; done

#disabling pageflip
echo 'Section "Device"'          | sudo tee -a /usr/share/X11/xorg.conf.d/20-displaylink.conf
echo 'Identifier "DisplayLink"'  | sudo tee -a /usr/share/X11/xorg.conf.d/20-displaylink.conf
echo 'Driver "modesetting"'      | sudo tee -a /usr/share/X11/xorg.conf.d/20-displaylink.conf
echo 'Option "PageFlip" "false"' | sudo tee -a /usr/share/X11/xorg.conf.d/20-displaylink.conf
echo "EndSection"                | sudo tee -a /usr/share/X11/xorg.conf.d/20-displaylink.conf

## Script to install VMWare + some fixes
yay -S vmware-workstation  --noconfirm --needed

sudo systemctl enable --now vmware-networks.service
sudo systemctl enable --now vmware-usbarbitrator.service
sudo systemctl enable --now vmware-hostd.service
sudo modprobe -a vmw_vmci vmmon
vmware 

#Fix for missing 3D support in VMware
echo "mks.gl.allowBlacklistedDrivers = "TRUE"" | tee -a /home/stefan/.vmware/preferences
sudo modprobe -a vmw_vmci vmmon

# Wallpapers
git clone https://gitlab.com/dwt1/wallpapers.git
mv wallpapers $HOME/Bilder
variety

echo "enter User:"
read USER
# Customizing Terminalprompt # 
touch "$HOME/.cache/zshhistory"
#-- Setup Alias in $HOME/zsh/aliasrc
git clone --depth=1 https://github.com/romkatv/powerlevel10k.git ~/powerlevel10k
git clone https://gitlab.com/stefanleis1/autoinstall/arch.git
echo 'source ~/powerlevel10k/powerlevel10k.zsh-theme' >> ~/.zshrc
sudo  usermod -s /bin/zsh ${USER}
mv /home/${USER}/.zshrc /home/${USER}/.zshrc.latest
cp /home/${USER}/arch/.zshrc /home/${USER}/.zshrc

# fork on manilaromes thunderblurred - https://github.com/manilarome/thunderblurred.git
git clone https://github.com/manilarome/thunderblurred.git
bash -c "$(curl -fsSL https://raw.githubusercontent.com/manilarome/thunderblurred/master/install.sh)"

echo -e "Open Thunderbird and enable 'Dark Mode' in Preferences for it to work"

### some flatpak programs
flatpak install flathub com.valvesoftware.Steam -y
flatpak install flathub io.github.liberodark.OpenDrive -y

### Geany themes
cd ${HOME}
git clone https://github.com/geany/geany-themes.git
cd geany-themes


