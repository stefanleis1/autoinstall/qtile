#!/usr/bin/env bash

#################### Setting Up Some Basic Variables ###################################
lsblk -f
read -p "Please enter disk [/dev/sda]" DISK
DISK=${DISK:-/dev/sda}
read -p "Please enter desired username [stefan]:" USER
USER=${USER:-stefan}
read -p "Please enter domain [fritz.box]" DOMAIN
DOMAIN=${DOMAIN:-fritz.box}
echo "Please enter computerame:"
read COMPUTERNAME
################### User- and Root-Account Configuration #############################
echo -e "\nRoot password configuration:\n$HR"
echo "Enter password for root user: "
passwd root
echo -e "\nUser password configuration:\n$HR"
useradd -m ${USER}
passwd ${USER}
usermod -aG users,wheel,audio,video,optical,storage ${USER}
mkdir /home/${USER}/Bilder
mkdir /home/${USER}/Musik
mkdir /home/${USER}/Videos
mkdir /home/${USER}/Dokumente
mkdir /home/${USER}/Downloads
chown ${USER}:${USER} /home/${USER}/Bilder
chown ${USER}:${USER} /home/${USER}/Musik
chown ${USER}:${USER} /home/${USER}/Videos
chown ${USER}:${USER} /home/${USER}/Dokumente
chown ${USER}:${USER} /home/${USER}/Downloads
########### Setup Language to DE and set locale ##############
ln -sf /usr/share/zoneinfo/Europe/Berlin /etc/localtime
hwclock --systohc
touch /etc/locale.conf
echo "LANG=de_DE.UTF-8"  | tee -a /etc/locale.conf
touch /etc/vconsole.conf
echo "KEYMAP=de-latin1"  | tee -a /etc/vconsole.conf
echo "FONT=eurlatgr"     | tee -a /etc/vconsole.conf  
echo "de_DE.UTF-8 UTF-8" | tee -a /etc/locale.gen
locale-gen
timedatectl --no-ask-password set-ntp 1
################ Some Mandatory Additional Encryption Configuration
echo -e "\nWriting EncryptionKeys To LUKS Headers:\n$HR"
dd bs=512 count=4 if=/dev/random of=/etc/key iflag=fullblock
chmod 600 /etc/key
cryptsetup -v luksAddKey "${DISK}2" /etc/key && cryptsetup -v luksAddKey "${DISK}3" /etc/key
##################### Add sudo no password rights ##############################################
sed -i 's/^# %wheel ALL=(ALL) NOPASSWD: ALL/%wheel ALL=(ALL) NOPASSWD: ALL/' /etc/sudoers
echo "${COMPUTERNAME}"                 | tee -a /etc/hostname
echo "127.0.0.1   localhost"           | tee -a /etc/hosts
echo "::1		  localhost"           | tee -a /etc/hosts
echo "127.0.1.1	  ${COMPUTERNAME}.${DOMAIN} ${COMPUTERNAME}" | tee -a /etc/hosts
####################### Bootloader Installation and Configuration #############################################
pacman -S grub dosfstools grub-btrfs os-prober mtools efibootmgr --noconfirm --needed
mv /etc/mkinitcpio.conf /etc/mkinitcpio.conf.bak
cp /qtile/btrfs/mkinitcpio.conf /etc/mkinitcpio.conf
mkinitcpio -P
mv /etc/default/grub /etc/default/grub.bak
cp /qtile/btrfs/grub /etc/default/grub
sed -i s/%uuid2%/$(blkid -o value -s UUID ${DISK}2)/ /etc/default/grub
sed -i s/%uuid3%/$(blkid -o value -s UUID ${DISK}3)/ /etc/default/grub
grub-install --target=x86_64-efi  --bootloader-id=GRUB
grub-mkconfig -o /boot/grub/grub.cfg
#######################  Network Setup #########################################################
pacman -S networkmanager wpa_supplicant wireless_tools netctl dialog --noconfirm --needed
systemctl enable --now NetworkManager
#################### Continuation with Package Installation #########################################
git clone https://github.com/arcolinux/arcolinux-spices
bash arcolinux-spices/usr/share/arcolinux-spices/scripts/get-the-keys-and-repos.sh
pacman -Syy
bash /qtile/btrfs/packages.sh



