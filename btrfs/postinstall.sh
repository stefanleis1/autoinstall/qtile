#!/usr/bin/env bash
echo -e "\nFINAL SETUP AND CONFIGURATION"

# ------------------------------------------------------------------------

echo -e "\nUpdating /bin/startx to use the correct path"

# By default, startx incorrectly looks for the .serverauth file in our HOME folder.
sudo sed -i 's|xserverauthfile=\$HOME/.serverauth.\$\$|xserverauthfile=\$XAUTHORITY|g' /bin/startx


# ------------------------------------------------------------------------

echo -e "\nDisabling buggy cursor inheritance"

# When you boot with multiple monitors the cursor can look huge. This fixes it.
sudo cat <<EOF > /usr/share/icons/default/index.theme
[Icon Theme]
#Inherits=Theme
EOF

#-------------------------------------------------------------------------

echo -e "\nAdding German KeyboardLayout to System"
#German keyboard Layout

localectl --no-convert set-x11-keymap de pc104 ,dvorak grp:alt_shift_toggle

echo Section '"InputClass"'                            | sudo tee -a /etc/X11/xorg.conf.d/00-keyboard.conf
echo Identifier '"system-keyboard"'                    | sudo tee -a /etc/X11/xorg.conf.d/00-keyboard.conf
echo MatchIsKeyboard '"on"'                            | sudo tee -a /etc/X11/xorg.conf.d/00-keyboard.conf
echo Option '"XkbLayout"' '"de"'                       | sudo tee -a /etc/X11/xorg.conf.d/00-keyboard.conf
echo EndSection                                        | sudo tee -a /etc/X11/xorg.conf.d/00-keyboard.conf

# ------------------------------------------------------------------------

echo -e "\nIncreasing file watcher count"

# This prevents a "too many files" error in Visual Studio Code
echo fs.inotify.max_user_watches=524288 | sudo sudo tee /etc/sysctl.d/40-max-user-watches.conf && sudo sysctl --system

# ------------------------------------------------------------------------

echo -e "\nDisabling Pulse .esd_auth module"

# Pulse audio loads the `esound-protocol` module, which best I can tell is rarely needed.
# That module creates a file called `.esd_auth` in the home directory which I'd prefer to not be there. So...
sudo sed -i 's|load-module module-esound-protocol-unix|#load-module module-esound-protocol-unix|g' /etc/pulse/default.pa

# ------------------------------------------------------------------------

echo -e "\nEnabling SDDM"

sudo systemctl enable sddm.service
sudo mkdir /etc/sddm.conf.d/ && sudo cp usr/lib/sddm/sddm.conf.d/default.conf /etc/sddm.conf.d/
git clone https://github.com/arcolinux/arcolinux-spices
sudo bash /usr/share/arcolinux-spices/scripts/get-the-keys-and-repos.sh
sudo pacman -S arcolinux-sddm-sugar-candy-git
sudo sed -i '/Current/s/=/=arcolinux-sugar-candy/' /etc/sddm.conf.d/default.conf
# ------------------------------------------------------------------------

# Some manual Systemd-Services

##Optimized Battery-setting
sudo touch /etc/systemd/system/powertop.service
echo [Unit]                                  | sudo tee -a /etc/systemd/system/powertop.service
echo Description=Powertop tunings            | sudo tee -a /etc/systemd/system/powertop.service
echo [Service]                               | sudo tee -a /etc/systemd/system/powertop.service
echo Type=oneshot                            | sudo tee -a /etc/systemd/system/powertop.service
echo ExecStart=/usr/bin/powertop --auto-tune | sudo tee -a /etc/systemd/system/powertop.service
echo [Install]                               | sudo tee -a /etc/systemd/system/powertop.service
echo WantedBy=multi-user.target              | sudo tee -a /etc/systemd/system/powertop.service

##Auto-Update Pacman
sudo touch /etc/systemd/system/autoupdate-pacman.service
echo [Unit]                                      | sudo tee -a /etc/systemd/system/autoupdate-pacman.service
echo Description=Automatic Update                | sudo tee -a /etc/systemd/system/autoupdate-pacman.service
echo After=network-online.target                 | sudo tee -a /etc/systemd/system/autoupdate-pacman.service
echo [Service]                                   | sudo tee -a /etc/systemd/system/autoupdate-pacman.service
echo Type=simple                                 | sudo tee -a /etc/systemd/system/autoupdate-pacman.service
echo ExecStart=/usr/bin/pacman -Syuq --noconfirm | sudo tee -a /etc/systemd/system/autoupdate-pacman.service
echo TimeoutStopSec=180                          | sudo tee -a /etc/systemd/system/autoupdate-pacman.service
echo KillMode=process                            | sudo tee -a /etc/systemd/system/autoupdate-pacman.service
echo KillSignal=SIGINT                           | sudo tee -a /etc/systemd/system/autoupdate-pacman.service
echo [Install]                                   | sudo tee -a /etc/systemd/system/autoupdate-pacman.service
echo WantedBy=multi-user.target                  | sudo tee -a /etc/systemd/system/autoupdate-pacman.service

sudo touch /etc/systemd/system/autoupdate-pacman.timer
echo [Unit]                                                      |sudo tee -a /etc/systemd/system/autoupdate-pacman.timer
echo Description=Automatic Update when booted up after 2 minutes |sudo tee -a /etc/systemd/system/autoupdate-pacman.timer
echo [Timer]                                                     |sudo tee -a /etc/systemd/system/autoupdate-pacman.timer
echo OnBootSec=2min                                              |sudo tee -a /etc/systemd/system/autoupdate-pacman.timer
echo OnUnitActiveSec=60min                                       |sudo tee -a /etc/systemd/system/autoupdate-pacman.timer
echo Unit=autoupdate-pacman.service                              |sudo tee -a /etc/systemd/system/autoupdate-pacman.timer
echo [Install]                                                   |sudo tee -a /etc/systemd/system/autoupdate-pacman.timer
echo WantedBy=multi-user.target                                  |sudo tee -a /etc/systemd/system/autoupdate-pacman.timer

sudo systemctl enable /etc/systemd/system/autoupdate-pacman.timer

##Auto-Update AUR
sudo touch /etc/systemd/system/autoupdate-yay.service
echo [Unit]                                      | sudo tee -a /etc/systemd/system/autoupdate-yay.service
echo Description=Automatic Update                | sudo tee -a /etc/systemd/system/autoupdate-yay.service
echo After=network-online.target                 | sudo tee -a /etc/systemd/system/autoupdate-yay.service
echo [Service]                                   | sudo tee -a /etc/systemd/system/autoupdate-yay.service
echo Type=simple                                 | sudo tee -a /etc/systemd/system/autoupdate-yay.service
echo ExecStart=/usr/bin/yay -Syuq --noconfirm    | sudo tee -a /etc/systemd/system/autoupdate-yay.service
echo TimeoutStopSec=180                          | sudo tee -a /etc/systemd/system/autoupdate-yay.service
echo KillMode=process                            | sudo tee -a /etc/systemd/system/autoupdate-yay.service
echo KillSignal=SIGINT                           | sudo tee -a /etc/systemd/system/autoupdate-yay.service
echo [Install]                                   | sudo tee -a /etc/systemd/system/autoupdate-yay.service
echo WantedBy=multi-user.target                  | sudo tee -a /etc/systemd/system/autoupdate-yay.service

touch /etc/systemd/system/autoupdate-yay.timer
echo [Unit]                                                      |sudo tee -a /etc/systemd/system/autoupdate-yay.timer
echo Description=Automatic Update when booted up after 2 minutes |sudo tee -a /etc/systemd/system/autoupdate-yay.timer
echo [Timer]                                                     |sudo tee -a /etc/systemd/system/autoupdate-yay.timer
echo OnBootSec=2min                                              |sudo tee -a /etc/systemd/system/autoupdate-yay.timer
echo OnUnitActiveSec=60min                                       |sudo tee -a /etc/systemd/system/autoupdate-yay.timer
echo Unit=autoupdate-yay.service                                 |sudo tee -a /etc/systemd/system/autoupdate-yay.timer
echo [Install]                                                   |sudo tee -a /etc/systemd/system/autoupdate-yay.timer
echo WantedBy=multi-user.target                                  |sudo tee -a /etc/systemd/system/autoupdate-yay.timer
sudo systemctl enable /etc/systemd/system/autoupdate-yay.timer

##Auto-Update Flatpak
sudo touch /etc/systemd/system/autoupdate-flatpak.service
echo [Unit]                                      | sudo tee -a /etc/systemd/system/autoupdate-flatpak.service
echo Description=Automatic Update                | sudo tee -a /etc/systemd/system/autoupdate-flatpak.service
echo After=network-online.target                 | sudo tee -a /etc/systemd/system/autoupdate-flatpak.service
echo [Service]                                   | sudo tee -a /etc/systemd/system/autoupdate-flatpak.service
echo Type=simple                                 | sudo tee -a /etc/systemd/system/autoupdate-flatpak.service
echo ExecStart=/usr/bin/flatpak upgrade          | sudo tee -a /etc/systemd/system/autoupdate-flatpak.service
echo TimeoutStopSec=180                          | sudo tee -a /etc/systemd/system/autoupdate-flatpak.service
echo KillMode=process                            | sudo tee -a /etc/systemd/system/autoupdate-flatpak.service
echo KillSignal=SIGINT                           | sudo tee -a /etc/systemd/system/autoupdate-flatpak.service
echo [Install]                                   | sudo tee -a /etc/systemd/system/autoupdate-flatpak.service
echo WantedBy=multi-user.target                  | sudo tee -a /etc/systemd/system/autoupdate-flatpak.service

touch /etc/systemd/system/autoupdate-flatpak.timer
echo [Unit]                                                      |sudo tee -a /etc/systemd/system/autoupdate-flatpak.timer
echo Description=Automatic Update when booted up after 2 minutes |sudo tee -a /etc/systemd/system/autoupdate-flatpak.timer
echo [Timer]                                                     |sudo tee -a /etc/systemd/system/autoupdate-flatpak.timer
echo OnBootSec=2min                                              |sudo tee -a /etc/systemd/system/autoupdate-flatpak.timer
echo OnUnitActiveSec=60min                                       |sudo tee -a /etc/systemd/system/autoupdate-flatpak.timer
echo Unit=autoupdate-flatpak.service                             |sudo tee -a /etc/systemd/system/autoupdate-flatpak.timer
echo [Install]                                                   |sudo tee -a /etc/systemd/system/autoupdate-flatpak.timer
echo WantedBy=multi-user.target                                  |sudo tee -a /etc/systemd/system/autoupdate-flatpak.timer
sudo systemctl enable /etc/systemd/system/autoupdate-flatpak.timer
# ------------------------------------------------------------------------
echo -e "\nEnabling xbacklight in Qtile for use with Keybinding and Widget"

touch /etc/X11/xorg.conf.d/20-intel.conf
echo Section     '"Device"'                             | sudo tee -a /etc/X11/xorg.conf.d/20-intel.conf
echo Identifier  '"Intel Graphics"'                     | sudo tee -a /etc/X11/xorg.conf.d/20-intel.conf
echo Driver      '"intel"'                              | sudo tee -a /etc/X11/xorg.conf.d/20-intel.conf
echo Option      '"Backlight"'  '"intel_backlight"'     | sudo tee -a /etc/X11/xorg.conf.d/20-intel.conf
echo EndSection                                         | sudo tee -a /etc/X11/xorg.conf.d/20-intel.conf

sudo touch /etc/udev/rules.d/backlight.rules
echo 'ACTION=="add"', 'SUBSYSTEM=="backlight"' KERNEL=='"intel_backlight', GROUP='"video"', MODE='"0664"' | sudo tee -a sudo tee -a /etc/udev/rules.d/backlight.rules
# ------------------------------------------------------------------------

echo -e "\n Enabling bluetooth daemon and setting it to auto-start"

sudo sed -i 's|#AutoEnable=false|AutoEnable=true|g' /etc/bluetooth/main.conf
sudo systemctl enable --now bluetooth.service

# ------------------------------------------------------------------------
echo -e "\nEnabling CPU-Frequence controlling"

sudo systemctl enable auto-cpufreq
sudo systemctl enable iwd

echo -e "\nEnabling the cups service daemon so we can print"

systemctl enable --now org.cups.cupsd.service
sudo ntpd -qg
sudo systemctl enable --now ntpd.service
sudo systemctl disable dhcpcd.service
sudo systemctl stop dhcpcd.service
sudo systemctl enable --now NetworkManager.service

################# Harden Linux ################################################
# check for unsigned kernel modules
for mod in $(lsmod | tail -n +2 | cut -d' ' -f1); do modinfo ${mod} | grep -q "signature" || echo "no signature for module: ${mod}" ; done

#-----------------------
#--Required Packages-
#-ufw
#-fail2ban

#-start firewall
sudo systemctl enable ufw

# --- Setup UFW rules
sudo ufw limit 22/tcp  
sudo ufw allow 80/tcp  
sudo ufw allow 443/tcp  
sudo ufw allow 422/tcp
sudo ufw default deny incoming  
sudo ufw default allow outgoing
sudo ufw enable

# --- Harden /etc/sysctl.conf
#sudo sysctl kernel.modules_disabled=1
sudo sysctl -a
sudo sysctl -A
sudo sysctl mib
sudo sysctl net.ipv4.conf.all.rp_filter
sudo sysctl -a --pattern 'net.ipv4.conf.(eth|wlan)0.arp'

# --- PREVENT IP SPOOFS
cat <<EOF > /etc/host.conf
order bind,hosts
multi on
EOF

# --- Enable fail2ban
sudo touch /etc/fail2ban/jail.local
echo "[DEFAULT]"                  | sudo tee -a /etc/fail2ban/jail.local
echo "ignoreip = 127.0.0.1/8 ::1" | sudo tee -a /etc/fail2ban/jail.local
echo "bantime = 3600h"            | sudo tee -a /etc/fail2ban/jail.local
echo "banaction = ufw"            | sudo tee -a /etc/fail2ban/jail.local
echo "findtime = 600h"            | sudo tee -a /etc/fail2ban/jail.local
echo "maxretry = 5"               | sudo tee -a /etc/fail2ban/jail.local
echo ""                           | sudo tee -a /etc/fail2ban/jail.local
echo "[sshd]"                     | sudo tee -a /etc/fail2ban/jail.local
echo "enabled = true"             | sudo tee -a /etc/fail2ban/jail.local
sudo mkdir /var/log/fail2ban/

sudo mkdir /etc/systemd/system/fail2ban.service.d
sudo touch /etc/systemd/system/fail2ban.service.d/override.conf
echo "[Service]"                                                                          | sudo tee -a /etc/systemd/system/fail2ban.service.d/override.conf
echo "PrivateDevices=yes"                                                                 | sudo tee -a /etc/systemd/system/fail2ban.service.d/override.conf
echo "PrivateTmp=yes"                                                                     | sudo tee -a /etc/systemd/system/fail2ban.service.d/override.conf
echo "ProtectHome=read-only"                                                              | sudo tee -a /etc/systemd/system/fail2ban.service.d/override.conf
echo "ProtectSystem=strict"                                                               | sudo tee -a /etc/systemd/system/fail2ban.service.d/override.conf
echo "ReadWritePaths=-/var/run/fail2ban"                                                  | sudo tee -a /etc/systemd/system/fail2ban.service.d/override.conf
echo "ReadWritePaths=-/var/lib/fail2ban"                                                  | sudo tee -a /etc/systemd/system/fail2ban.service.d/override.conf
echo "ReadWritePaths=-/var/log/fail2ban"                                                  | sudo tee -a /etc/systemd/system/fail2ban.service.d/override.conf
echo "ReadWritePaths=-/var/spool/postfix/maildrop"                                        | sudo tee -a /etc/systemd/system/fail2ban.service.d/override.conf
echo "ReadWritePaths=-/run/xtables.lock"                                                  | sudo tee -a /etc/systemd/system/fail2ban.service.d/override.conf
echo "CapabilityBoundingSet=CAP_AUDIT_READ CAP_DAC_READ_SEARCH CAP_NET_ADMIN CAP_NET_RAW" | sudo tee -a /etc/systemd/system/fail2ban.service.d/override.conf

sudo touch /etc/fail2ban/jail.d/sshd.local
echo "[sshd]"                  | sudo tee -a /etc/fail2ban/jail.d/sshd.local
echo "enabled   = true"        | sudo tee -a /etc/fail2ban/jail.d/sshd.local
echo "filter    = sshd"        | sudo tee -a /etc/fail2ban/jail.d/sshd.local
echo "banaction = ufw"         | sudo tee -a /etc/fail2ban/jail.d/sshd.local
echo "backend   = systemd"     | sudo tee -a /etc/fail2ban/jail.d/sshd.local
echo "maxretry  = 5"           | sudo tee -a /etc/fail2ban/jail.d/sshd.local
echo "findtime  = 1d"          | sudo tee -a /etc/fail2ban/jail.d/sshd.local
echo "bantime   = 2w"          | sudo tee -a /etc/fail2ban/jail.d/sshd.local
echo "ignoreip  = 127.0.0.1/8" | sudo tee -a /etc/fail2ban/jail.d/sshd.local

sudo systemctl enable fail2ban
sudo systemctl start fail2ban

echo "listening ports"
sudo netstat -tunlp
###############################################################################################
echo "
###############################################################################
# Cleaning
###############################################################################
"
# Remove no password sudo rights
sed -i 's/^%wheel ALL=(ALL) NOPASSWD: ALL/# %wheel ALL=(ALL) NOPASSWD: ALL/' /etc/sudoers
# Add sudo rights
sed -i 's/^# %wheel ALL=(ALL) ALL/%wheel ALL=(ALL) ALL/' /etc/sudoers

# Clean orphans pkg
if [[ ! -n $(pacman -Qdt) ]]; then
	echo "No orphans to remove."
else
	pacman -Rns $(pacman -Qdtq)
fi

# Replace in the same state
cd $pwd

echo "
###############################################################################
# Done
###############################################################################
"
