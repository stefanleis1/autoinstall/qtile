#!/usr/bin/env bash

echo "------------------------------------------"
echo "-- Arch Base Configuration on Main Drive--"
echo "------------------------------------------"
#################### Setting Up Some Basic Variables ###################################

lsblk -f
echo "Please enter disk: (f.e.: /dev/sda)"
read DISK
echo "Please enter desired username: (f.e.: stefan)"
read USER
echo "Please enter domain: (f.e.: fritz.box)"
read DOMAIN

################### User- and Root-Account Configuration #############################

echo "--------------------------------------------"
echo "--- User- and Root-Account Configuration ---"
echo "--------------------------------------------"

echo -e "\nRoot password configuration:\n$HR"
echo "Enter password for root user: "
passwd root

echo -e "\nUser password configuration:\n$HR"
useradd -m ${USER}
passwd ${USER}
usermod -aG users,wheel,audio,video,optical,storage ${USER}
mkdir /home/${USER}/Bilder
mkdir /home/${USER}/Musik
mkdir /home/${USER}/Videos
mkdir /home/${USER}/Dokumente
mkdir /home/${USER}/Downloads
chown ${USER}:${USER} /home/${USER}/Bilder
chown ${USER}:${USER} /home/${USER}/Musik
chown ${USER}:${USER} /home/${USER}/Videos
chown ${USER}:${USER} /home/${USER}/Dokumente
chown ${USER}:${USER} /home/${USER}/Downloads

echo "-------------------------------------------------"
echo "       Setup Language to DE and set locale       "
echo "-------------------------------------------------"

ln -sf /usr/share/zoneinfo/Europe/Berlin /etc/localtime
hwclock --systohc
touch /etc/locale.conf
echo "LANG=de_DE.UTF-8"  | tee -a /etc/locale.conf
touch /etc/vconsole.conf
echo "KEYMAP=de-latin1"  | tee -a /etc/vconsole.conf
echo "FONT=eurlatgr"     | tee -a /etc/vconsole.conf  
echo "de_DE.UTF-8 UTF-8" | tee -a /etc/locale.gen
locale-gen
timedatectl --no-ask-password set-ntp 1

################ Some Mandatory Additional Encryption Configuration

echo -e "\nWriting EncryptionKeys To LUKS Headers:\n$HR"
dd bs=512 count=4 if=/dev/random of=/etc/key iflag=fullblock
chmod 600 /etc/key
cryptsetup -v luksAddKey "${DISK}p2" /etc/key && cryptsetup -v luksAddKey "${DISK}p3" /etc/key
###################### Add sudo no password rights ##############################################

sed -i 's/^# %wheel ALL=(ALL) NOPASSWD: ALL/%wheel ALL=(ALL) NOPASSWD: ALL/' /etc/sudoers
echo "Stefan-Thinkpad"                 | tee -a /etc/hostname
echo "127.0.0.1   localhost"           | tee -a /etc/hosts
echo "::1		  localhost"           | tee -a /etc/hosts
echo "127.0.1.1	  Stefan.Thinkpad.${DOMAIN} Stefan-Thinkpad" | tee -a /etc/hosts


#######################  Bootloader Installation and Configuration #############################################

echo "------------------------------------"
echo "-- Bootloader Grub Installation-----"
echo "------------------------------------"

pacman -S grub dosfstools grub-btrfsos-prober mtools --noconfirm --needed
mv /etc/mkinitcpio.conf /etc/mkinitcpio.conf.bak
cp /qtile/mkinitcpio.conf /etc/mkinitcpio.conf
mkinitcpio -P
mv /etc/default/grub /etc/default/grub.bak
cp /qtile/grub /etc/default/grub
sed -i s/%uuid2%/$(blkid -o value -s UUID ${DISK}2)/ /etc/default/grub
sed -i s/%uuid3%/$(blkid -o value -s UUID ${DISK}3)/ /etc/default/grub
grub-install --target=x86_64-efi  --bootloader-id=GRUB
grub-mkconfig -o /boot/grub/grub.cfg

#######################  Network Setup #########################################################

echo "--------------------------------------"
echo "--          Network Setup           --"
echo "--------------------------------------"

pacman -S networkmanager wpa_supplicant wireless_tools netctl dialog --noconfirm --needed
systemctl enable --now NetworkManager

#################### Continue With Package Installation #########################################
bash /qtile/packages.sh


